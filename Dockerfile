FROM codercom/code-server:latest

USER root

RUN apt-get update \
  && apt-get install -y \
  nodejs \
  npm \
  httpie \
  && rm -rf /var/lib/apt/lists/*

RUN npm install -g now

# https://wiki.debian.org/Locale#Manually
RUN sed -i "s/# de_DE.UTF-8/de_DE.UTF-8/" /etc/locale.gen \
  && locale-gen
ENV LANG=de_DE.UTF-8

USER coder
